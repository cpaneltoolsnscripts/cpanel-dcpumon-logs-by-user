#!/bin/bash
## Author: Michael Ramsey
## Objective Find cPanel user's highest CPU & Memory use processes via Dcpumon Logs for Past 5 Days
## How to use.
## ./cPaneldcpumonLogs_past5.sh username
Username=$1

CURRENTDATE=$(date +"%Y-%m-%d %T") # 2019-02-09 06:47:56
PreviousDay1=$(date --date='1 day ago' +"%Y-%m-%d")  # 2019-02-08
PreviousDay2=$(date --date='2 days ago' +"%Y-%m-%d") # 2019-02-07
PreviousDay3=$(date --date='3 days ago' +"%Y-%m-%d") # 2019-02-06
PreviousDay4=$(date --date='4 days ago' +"%Y-%m-%d") # 2019-02-05

datetimeDcpumon=$(date +"%Y/%b/%d") # 2019/Feb/15
datetimeDcpumon1DaysAgo=$(date --date='1 day ago' +"%Y/%b/%d")  # 2019/Feb/14
datetimeDcpumon2DaysAgo=$(date --date='2 days ago' +"%Y/%b/%d") # 2019/Feb/13
datetimeDcpumon3DaysAgo=$(date --date='3 days ago' +"%Y/%b/%d") # 2019/Feb/12
datetimeDcpumon4DaysAgo=$(date --date='4 days ago' +"%Y/%b/%d") # 2019/Feb/11


#sudo grep $Username /var/log/dcpumon/2019/Feb/15
echo "============================================================="
echo "Find $Username user's highest CPU use processes via Dcpumon Logs for ${CURRENTDATE}"
sudo grep $Username /var/log/dcpumon/$datetimeDcpumon
echo "============================================================="

#Past few days stats
echo "Find $Username user's highest CPU use processes via Dcpumon Logs for ${PreviousDay1}"
sudo grep $Username /var/log/dcpumon/$datetimeDcpumon1DaysAgo
echo "============================================================="
echo "Find $Username user's highest CPU use processes via Dcpumon Logs for ${PreviousDay2}"
sudo grep $Username /var/log/dcpumon/$datetimeDcpumon2DaysAgo
echo "============================================================="
echo "Find $Username user's highest CPU use processes via Dcpumon Logs for ${PreviousDay3}"
sudo grep $Username /var/log/dcpumon/$datetimeDcpumon3DaysAgo
echo "============================================================="
echo "Find $Username user's highest CPU use processes via Dcpumon Logs for ${PreviousDay4}"
sudo grep $Username /var/log/dcpumon/$datetimeDcpumon4DaysAgo
echo "============================================================="